package io.konv.airline.domain.service.impl;

import io.konv.airline.domain.AirlineRepository;
import io.konv.airline.domain.entities.aircraft.Aircraft;
import io.konv.airline.domain.entities.aircraft.Airplane;
import io.konv.airline.domain.entities.aircraft.Balloon;
import io.konv.airline.domain.entities.aircraft.Helicopter;
import io.konv.airline.domain.entities.airline.Airline;
import io.konv.airline.domain.entities.airline.SimpleAirline;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.*;

public class DefaultAirlineInfoServiceTest {

    private AirlineRepository repository;
    private DefaultAirlineInfoService subject;

    @Before
    public void setUp() {
        repository = Mockito.mock(AirlineRepository.class);
        subject = new DefaultAirlineInfoService(repository);
    }

    @Test
    public void shouldReturnCorrectAirlineNamesSet() {
        Airline first = new SimpleAirline();
        Airline second = new SimpleAirline();
        first.setId(5);
        first.setName("Cool Airline");
        second.setId(45);
        second.setName("Fun Airline");
        Set<Airline> airlines = new HashSet<>();
        airlines.add(first);
        airlines.add(second);
        Mockito.when(repository.getAllAirlines()).thenReturn((Set) airlines);

        Set<? extends Airline> actual = subject.getAllAirlines();

        assertThat(actual.stream().mapToLong(Airline::getId).sum()).isEqualTo(50);
        assertThat(actual.stream().map(Airline::getName).collect(Collectors.toSet())).containsExactly("Cool Airline", "Fun Airline");
    }

    @Test
    public void shouldReturnCorrectAirlineById() {
        Airline airline = new SimpleAirline();
        airline.setId(567);
        airline.setName("Awesome Airline");
        Set<Airline> airlineSet = new HashSet<>();
        airlineSet.add(airline);
        Mockito.when(repository.getAllAirlines()).thenReturn((Set) airlineSet);

        Optional<? extends Airline> optionalAirline = subject.getAirlineById(567);

        assertThat(optionalAirline).isNotEmpty();
        assertThat(optionalAirline.get().getId()).isEqualTo(567);
        assertThat(optionalAirline.get().getName()).isEqualTo("Awesome Airline");
    }

    @Test
    public void getReturnCorrectTotalPassengerCapacity() {
        // Given
        Airline airline = new SimpleAirline();
        airline.setId(345);

        Aircraft first = new Balloon();
        Aircraft second = new Helicopter();
        Aircraft third = new Airplane();

        first.setPassengerCapacity(150);
        second.setPassengerCapacity(50);
        third.setPassengerCapacity(100);

        Set<Airline> airlineSet = new HashSet<>();
        airlineSet.add(airline);

        Set<Aircraft> aircraftSet = new HashSet<>();

        aircraftSet.add(first);
        aircraftSet.add(second);
        aircraftSet.add(third);

        Mockito.when(repository.getAllAirlines()).thenReturn((Set) airlineSet);
        Mockito.when(repository.getAircraftByAirlineId(345)).thenReturn(Optional.of(aircraftSet));

        // When
        Optional<Integer> optionalCapacity = subject.getTotalPassengerCapacity(345);

        // Then
        assertThat(optionalCapacity).isNotEmpty();
        assertThat(optionalCapacity.get()).isEqualTo(300);
    }

    @Test
    public void getTotalCarryingCapacityKg() {
        // Given
        Airline airline = new SimpleAirline();
        airline.setId(345);

        Aircraft first = new Balloon();
        Aircraft second = new Helicopter();
        Aircraft third = new Airplane();

        first.setCarryingCapacityKg(150);
        second.setCarryingCapacityKg(50);
        third.setCarryingCapacityKg(100);

        Set<Airline> airlineSet = new HashSet<>();
        airlineSet.add(airline);

        Set<Aircraft> aircraftSet = new HashSet<>();

        aircraftSet.add(first);
        aircraftSet.add(second);
        aircraftSet.add(third);

        Mockito.when(repository.getAllAirlines()).thenReturn((Set) airlineSet);
        Mockito.when(repository.getAircraftByAirlineId(345)).thenReturn(Optional.of(aircraftSet));

        // When
        Optional<Integer> optionalCapacity = subject.getTotalCarryingCapacityKg(345);

        // Then
        assertThat(optionalCapacity).isNotEmpty();
        assertThat(optionalCapacity.get()).isEqualTo(300);
    }

    @Test
    public void getAircraftByAirlineId() {
        // Given
        Airline airline = new SimpleAirline();
        airline.setId(345);

        Aircraft first = new Balloon();
        Aircraft second = new Helicopter();
        Aircraft third = new Airplane();

        first.setId(1);
        second.setId(2);
        third.setId(3);

        Set<Airline> airlineSet = new HashSet<>();
        airlineSet.add(airline);

        Set<Aircraft> aircraftSet = new HashSet<>();

        aircraftSet.add(first);
        aircraftSet.add(second);
        aircraftSet.add(third);

        Mockito.when(repository.getAllAirlines()).thenReturn((Set) airlineSet);
        Mockito.when(repository.getAircraftByAirlineId(345)).thenReturn(Optional.of(aircraftSet));

        // When
        Optional<Set<? extends Aircraft>> optionalAircraftSet = subject.getAircraftByAirlineId(345);

        // Then
        assertThat(optionalAircraftSet).isNotEmpty();
        assertThat(optionalAircraftSet.get().stream().mapToLong(Aircraft::getId).sum()).isEqualTo(6);
    }
}