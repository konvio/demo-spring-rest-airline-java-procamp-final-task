package io.konv.airline.domain.service;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AircraftSortingServiceSortingStrategyTest {

    @Test
    public void shouldReturnFlightRangeAscSortingStrategyForName() {
        AircraftSortingService.SortingStrategy actual = AircraftSortingService.SortingStrategy.forNameOrDefault("flight_range");
        assertThat(actual).isEqualTo(AircraftSortingService.SortingStrategy.FLIGHT_RANGE_ASC);
    }

    @Test
    public void shouldReturnDefaultSortingStrategyForName() {
        AircraftSortingService.SortingStrategy actual = AircraftSortingService.SortingStrategy.forNameOrDefault("default");
        assertThat(actual).isEqualTo(AircraftSortingService.SortingStrategy.DEFAULT);
    }

    @Test
    public void shouldReturnDefaultSortingStrategyForInvalidName() {
        AircraftSortingService.SortingStrategy actual = AircraftSortingService.SortingStrategy.forNameOrDefault("alldjfalkdfjlkejwljlsdajj");
        assertThat(actual).isEqualTo(AircraftSortingService.SortingStrategy.DEFAULT);
    }
}