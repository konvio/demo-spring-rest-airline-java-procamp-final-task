package io.konv.airline.domain.service.impl;

import io.konv.airline.data.dto.AircraftTableRow;
import io.konv.airline.domain.AirlineRepository;
import io.konv.airline.domain.entities.aircraft.Aircraft;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;

public class DefaultAircraftModificationServiceTest {

    private AirlineRepository repository;
    private DefaultAircraftModificationService subject;

    @Before
    public void setUp() {
        repository = Mockito.mock(AirlineRepository.class);
        subject = new DefaultAircraftModificationService(repository);
    }

    @Test
    public void shouldCallRepositoryDeleteOnDeleteAircraft() {
        subject.deleteAircraftIfPresent(42);
        Mockito.verify(repository, Mockito.atLeastOnce()).deleteAircraftIfPresent(42);
    }

    @Test
    public void shouldCallRepositoryAddWithCorrectIds() {
        AircraftTableRow aircraftTableRow = new AircraftTableRow();
        aircraftTableRow.setId(42);
        aircraftTableRow.setAirlineId(459);

        subject.addAircraft(aircraftTableRow);

        ArgumentCaptor<Aircraft> aircraftArgumentCaptor = ArgumentCaptor.forClass(Aircraft.class);
        ArgumentCaptor<Long> airlineIdArgumentCaptor = ArgumentCaptor.forClass(Long.class);
        Mockito.verify(repository).addAircraftToAirlineOrUpdate(aircraftArgumentCaptor.capture(), airlineIdArgumentCaptor.capture());
        assertThat(aircraftArgumentCaptor.getValue().getId()).isEqualTo(42);
        assertThat(airlineIdArgumentCaptor.getValue()).isEqualTo(459);
    }
}