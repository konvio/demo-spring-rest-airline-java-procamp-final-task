package io.konv.airline.domain.service.impl;

import io.konv.airline.domain.AirlineRepository;
import io.konv.airline.domain.entities.aircraft.Aircraft;
import io.konv.airline.domain.entities.aircraft.Airplane;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class DefaultAircraftInfoServiceTest {

    private AirlineRepository airlineRepository;
    private DefaultAircraftInfoService subject;

    @Before
    public void setUp() {
        airlineRepository = Mockito.mock(AirlineRepository.class);
        subject = new DefaultAircraftInfoService(airlineRepository);
    }

    @Test
    public void shouldReturnEmptyAircraftSetWhenRepositoryReturnEmptySet() {
        Mockito.when(airlineRepository.getAllAircraft()).thenReturn(Collections.emptySet());
        assertThat(subject.getAllAircraft()).isEmpty();
    }

    @Test
    public void shouldReturnEmptyOptionalWhenNoAircraftWithSpecifiedIdExists() {
        Mockito.when(airlineRepository.getAircraftByAirlineId(42)).thenReturn(Optional.empty());
        assertThat(subject.getAircraftById(42)).isEmpty();
    }

    @Test
    public void shouldReturnAircraftById() {
        Airplane airplane = new Airplane();
        airplane.setId(42);
        airplane.setName("Test Name");
        airplane.setPassengerCapacity(456);
        Mockito.when(airlineRepository.getAircraftById(42)).thenReturn((Optional) Optional.of(airplane));

        Aircraft actualAircraft = subject.getAircraftById(42).get();

        assertThat(actualAircraft.getId()).isEqualTo(42);
        assertThat(actualAircraft.getName()).isEqualTo("Test Name");
        assertThat(actualAircraft.getPassengerCapacity()).isEqualTo(456);
    }
}