package io.konv.airline.domain.service.impl;

import io.konv.airline.domain.entities.aircraft.Aircraft;
import io.konv.airline.domain.entities.aircraft.Airplane;
import io.konv.airline.domain.entities.aircraft.Balloon;
import io.konv.airline.domain.entities.aircraft.Helicopter;
import io.konv.airline.domain.service.AircraftSortingService;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class DefaultAircraftSortingServiceTest {

    private DefaultAircraftSortingService subject;

    @Before
    public void setUp() {
        subject = new DefaultAircraftSortingService();
    }

    @Test
    public void shouldSortWithFlightRangeAscSortingStrategy() {
        Aircraft first = new Helicopter();
        Aircraft second = new Airplane();
        Aircraft third = new Balloon();
        first.setId(1);
        second.setId(2);
        third.setId(3);
        first.setMaxFlightRangeKm(5000);
        second.setMaxFlightRangeKm(3000);
        third.setMaxFlightRangeKm(1000);
        List<Aircraft> aircraftList = new ArrayList<>(3);
        aircraftList.add(first);
        aircraftList.add(second);
        aircraftList.add(third);

        List<? extends Aircraft> actual = subject.sort(aircraftList, AircraftSortingService.SortingStrategy.FLIGHT_RANGE_ASC);

        assertThat(actual.get(0).getId()).isEqualTo(3);
        assertThat(actual.get(1).getId()).isEqualTo(2);
        assertThat(actual.get(2).getId()).isEqualTo(1);
    }

    @Test
    public void shouldSortByIdWithDefaultSortingStrategy() {
        Aircraft first = new Helicopter();
        Aircraft second = new Airplane();
        Aircraft third = new Balloon();
        first.setId(3);
        second.setId(2);
        third.setId(1);
        List<Aircraft> aircraftList = new ArrayList<>(3);
        aircraftList.add(first);
        aircraftList.add(second);
        aircraftList.add(third);

        List<? extends Aircraft> actual = subject.sort(aircraftList, AircraftSortingService.SortingStrategy.DEFAULT);

        assertThat(actual.get(0).getId()).isEqualTo(1);
        assertThat(actual.get(1).getId()).isEqualTo(2);
        assertThat(actual.get(2).getId()).isEqualTo(3);
    }
}