package io.konv.airline.data.mapper;

import io.konv.airline.data.dto.AirlineTableRow;
import io.konv.airline.domain.entities.airline.Airline;
import io.konv.airline.domain.entities.airline.SimpleAirline;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AirlineTableRowMapperTest {

    @Test
    public void shouldConvertToSimpleAirline() {
        AirlineTableRow airlineTableRow = new AirlineTableRow();
        airlineTableRow.setId(56);
        airlineTableRow.setName("Sample Name");

        Airline airline = AirlineTableRowMapper.convertTableRowToAirline(airlineTableRow);

        assertThat(airline.getId()).isEqualTo(56);
        assertThat(airline.getName()).isEqualTo("Sample Name");
    }

    @Test
    public void shouldConvertSimpleAirlineToAirlineTableRow() {
        Airline airline = new SimpleAirline();
        airline.setId(456);
        airline.setName("Airline");

        AirlineTableRow airlineTableRow = AirlineTableRowMapper.convertAirlineToTableRow(airline);

        assertThat(airlineTableRow.getId()).isEqualTo(456);
        assertThat(airlineTableRow.getName()).isEqualTo("Airline");
    }
}