package io.konv.airline.data.mapper;

import io.konv.airline.data.dto.AircraftTableRow;
import io.konv.airline.domain.entities.aircraft.Aircraft;
import io.konv.airline.domain.entities.aircraft.Airplane;
import io.konv.airline.domain.entities.aircraft.Balloon;
import io.konv.airline.domain.entities.aircraft.Helicopter;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class AircraftTableRowMapperTest {

    @Test
    public void shouldReturnCorrectAircraftTypesAccordingToTypeField() {
        AircraftTableRow airplaneRow = new AircraftTableRow();
        airplaneRow.setType("airplane");
        AircraftTableRow helicopterRow = new AircraftTableRow();
        helicopterRow.setType("helicopter");
        AircraftTableRow balloonRow = new AircraftTableRow();
        balloonRow.setType("balloon");

        Aircraft airplane = AircraftTableRowMapper.convertTableRowToAircraft(airplaneRow);
        Aircraft helicopter = AircraftTableRowMapper.convertTableRowToAircraft(helicopterRow);
        Aircraft balloon = AircraftTableRowMapper.convertTableRowToAircraft(balloonRow);

        assertThat(airplane).isInstanceOf(Airplane.class);
        assertThat(helicopter).isInstanceOf(Helicopter.class);
        assertThat(balloon).isInstanceOf(Balloon.class);
    }

    @Test
    public void shouldSetConcreteAircraftSpecificField() {
        Airplane airplane = new Airplane();
        airplane.setWingspanSm(100);
        Helicopter helicopter = new Helicopter();
        helicopter.setMaxRevolutionsPerMinute(2000);
        Balloon balloon = new Balloon();
        balloon.setGasVolume(500);

        AircraftTableRow airplaneRow = AircraftTableRowMapper.convertAircraftToTableRow(airplane, 0);
        AircraftTableRow helicopterRow = AircraftTableRowMapper.convertAircraftToTableRow(helicopter, 0);
        AircraftTableRow balloonRow = AircraftTableRowMapper.convertAircraftToTableRow(balloon, 0);

        assertThat(airplaneRow.getWingspan()).isEqualTo(100);
        assertThat(helicopterRow.getMaxRevolutions()).isEqualTo(2000);
        assertThat(balloonRow.getGasVolume()).isEqualTo(500);
    }
}