package io.konv.airline.data;

import io.konv.airline.data.dao.AircraftDao;
import io.konv.airline.data.dao.AirlineDao;
import io.konv.airline.data.dto.AircraftTableRow;
import io.konv.airline.data.dto.AirlineTableRow;
import io.konv.airline.domain.entities.airline.Airline;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

public class DefaultAirlineRepositoryTest {

    private AirlineDao airlineDao;
    private AircraftDao aircraftDao;
    private DefaultAirlineRepository subject;

    @Before
    public void setUp() {
        airlineDao = Mockito.mock(AirlineDao.class);
        aircraftDao = Mockito.mock(AircraftDao.class);
        subject = new DefaultAirlineRepository(airlineDao, aircraftDao);
    }

    @Test
    public void shouldPopulateAirlineWithAircraft() {
        AirlineTableRow airlineTableRow = new AirlineTableRow();
        airlineTableRow.setId(42);
        Mockito.when(airlineDao.getAllAirlines()).thenReturn(Collections.singleton(airlineTableRow));
        AircraftTableRow aircraftTableRow = new AircraftTableRow();
        aircraftTableRow.setAirlineId(42);
        Mockito.when(aircraftDao.getAircraftByAirlineId(42)).thenReturn(Collections.singleton(aircraftTableRow));

        Airline airline = subject.getAllAirlines().stream().findFirst().get();

        assertThat(airline.getOwnedAircraft().size()).isPositive();
    }
}