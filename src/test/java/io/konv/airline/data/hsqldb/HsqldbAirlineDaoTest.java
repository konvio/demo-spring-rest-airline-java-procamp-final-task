package io.konv.airline.data.hsqldb;

import io.konv.airline.data.dto.AirlineTableRow;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HsqldbAirlineDaoTest {

    @Inject
    private HsqldbAirlineDao airlineDao;

    @Test
    public void shouldContainTestData() {
        assertThat(airlineDao.getAllAirlines().size()).isPositive();
    }

    @Test
    public void shouldDeleteRowById() {
        long sampleId = airlineDao.getAllAirlines().stream().findAny().get().getId();

        airlineDao.deleteAirlineById(sampleId);

        assertThat(airlineDao.getAirlineById(sampleId)).isEmpty();
    }

    @Test
    public void shouldInsertTableRow() {
        long sampleId = 4563;
        airlineDao.deleteAirlineById(sampleId);
        AirlineTableRow airlineTableRow = new AirlineTableRow();
        airlineTableRow.setId(sampleId);

        airlineDao.insertAirlineOrUpdate(airlineTableRow);

        assertThat(airlineDao.getAirlineById(sampleId)).isNotEmpty();
    }
}