package io.konv.airline.data.hsqldb;

import io.konv.airline.data.dto.AircraftTableRow;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HsqldbAircraftDaoTest {

    @Inject
    private HsqldbAircraftDao aircraftDao;

    @Test
    public void shouldContainTestData() {
        assertThat(aircraftDao.getAllAircraft().size()).isPositive();
    }

    @Test
    public void shouldDeleteRowById() {
        AircraftTableRow tableRow = aircraftDao.getAllAircraft().stream().findAny().get();

        aircraftDao.deleteAircraftById(tableRow.getId());

        assertThat(aircraftDao.getAircraftById(tableRow.getId())).isEmpty();
    }

    @Test
    public void shouldInsertAircraftTableRow() {
        AircraftTableRow aircraftTableRow = new AircraftTableRow();
        aircraftTableRow.setId(567);
        aircraftTableRow.setName("rocketX");

        aircraftDao.insertAircraftOrUpdate(aircraftTableRow);

        Optional<AircraftTableRow> optionalRow = aircraftDao.getAircraftById(567);
        assertThat(optionalRow).isNotEmpty();
        assertThat(optionalRow.get().getName()).isEqualTo("rocketX");
    }
}