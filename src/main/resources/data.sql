INSERT INTO airline (id, name) VALUES
  (0, 'Virgin Airlines'),
  (1, 'Balloon Inc.'),
  (2, 'JetBlue Airways');

INSERT INTO aircraft (
  airline_id,
  type,
  name,
  passenger_capacity,
  cargo_capacity,
  max_flight_range,
  remaining_fuel,
  fuel_consumption,
  wingspan
) VALUES
  (0, 'airplane', 'Boeing-747', 400, 3000, 2000, 400, 50, 6000),
  (0, 'airplane', 'Boeing-749', 500, 4000, 2500, 400, 50, 6500),
  (1, 'airplane', 'Boeing-737', 545, 3456, 2300, 600, 80, 6500),
  (1, 'airplane', 'Phoenix', 100, 1000, 3000, 834, 40, 5000),
  (2, 'airplane', 'Destiny', 400, 1500, 1300, 456, 40, 5348);

INSERT INTO aircraft (
  airline_id,
  type,
  name,
  passenger_capacity,
  cargo_capacity,
  max_flight_range,
  remaining_fuel,
  fuel_consumption,
  gas_volume
) VALUES
  (0, 'balloon', 'Happiness', 5, 340, 800, 400, 20, 6500),
  (1, 'balloon', 'Good Life', 7, 500, 1000, 300, 25, 7000),
  (2, 'balloon', 'Sky Walker', 6, 450, 900, 500, 23, 6700);

INSERT INTO aircraft (
  airline_id,
  type,
  name,
  passenger_capacity,
  cargo_capacity,
  max_flight_range,
  remaining_fuel,
  fuel_consumption,
  MAX_REVOLUTIONS
) VALUES
  (0, 'helicopter', 'Thunder', 4, 350, 4000, 1000, 34, 24000),
  (1, 'helicopter', 'Black Smith', 5, 500, 10000, 3000, 45, 26000),
  (2, 'helicopter', 'White Cat', 4, 400, 2500, 4353, 39, 23000);