package io.konv.airline.presentation.rest;

import io.konv.airline.domain.entities.aircraft.Aircraft;
import io.konv.airline.domain.entities.airline.Airline;
import io.konv.airline.domain.service.AircraftSortingService;
import io.konv.airline.domain.service.AirlineInfoService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
public class AirlineInfoRestController {

    private AirlineInfoService airlineInfoService;
    private AircraftSortingService aircraftSortingService;

    @Inject
    public AirlineInfoRestController(AirlineInfoService airlineInfoService, AircraftSortingService aircraftSortingService) {
        this.airlineInfoService = airlineInfoService;
        this.aircraftSortingService = aircraftSortingService;
    }

    @GetMapping(value = "/airline", produces = MediaType.APPLICATION_JSON_VALUE)
    public Set<? extends Airline> getAllAirlines() {
        return airlineInfoService.getAllAirlines();
    }

    @GetMapping(value = "/airline/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Airline getAirlineById(@PathVariable("id") long airlineId) {
        return airlineInfoService.getAirlineById(airlineId).orElse(null);
    }

    @GetMapping(value = "/airline/{id}/passenger-capacity", produces = MediaType.APPLICATION_JSON_VALUE)
    public Integer getTotalPassengerCapacity(@PathVariable("id") long airlineId) {
        return airlineInfoService.getTotalPassengerCapacity(airlineId).orElse(null);
    }

    @GetMapping(value = "/airline/{id}/cargo-capacity", produces = MediaType.APPLICATION_JSON_VALUE)
    public Integer getTotalCarryingCapacity(@PathVariable("id") long airlineId) {
        return airlineInfoService.getTotalCarryingCapacityKg(airlineId).orElse(null);
    }

    @GetMapping(value = "/airline/{id}/aircraft", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<? extends Aircraft> getAirlineAircraft(
            @PathVariable("id") long airlineId,
            @RequestParam(value = "sort", defaultValue = "flight_range") String sortingStrategyName) {
        Optional<Set<? extends Aircraft>> aircraft = airlineInfoService.getAircraftByAirlineId(airlineId);
        AircraftSortingService.SortingStrategy sortingStrategy = AircraftSortingService.SortingStrategy.forNameOrDefault(sortingStrategyName);
        return aircraft.map(aircraftSet -> aircraftSortingService.sort(aircraftSet, sortingStrategy)).orElse(null);
    }
}
