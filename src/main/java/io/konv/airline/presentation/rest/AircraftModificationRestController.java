package io.konv.airline.presentation.rest;

import io.konv.airline.data.dto.AircraftTableRow;
import io.konv.airline.domain.service.AircraftModificationService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
public class AircraftModificationRestController {

    private AircraftModificationService aircraftModificationService;

    @Inject
    public AircraftModificationRestController(AircraftModificationService aircraftModificationService) {
        this.aircraftModificationService = aircraftModificationService;
    }

    @DeleteMapping("/aircraft/{id}")
    public void deleteAircraftById(@PathVariable("id") long id) {
        aircraftModificationService.deleteAircraftIfPresent(id);
    }

    @PostMapping("/aircraft")
    public void addAircraft(@RequestBody AircraftTableRow tableRow) {
        aircraftModificationService.addAircraft(tableRow);
    }
}
