package io.konv.airline.presentation.rest;

import io.konv.airline.domain.entities.aircraft.Aircraft;
import io.konv.airline.domain.service.AircraftInfoService;
import io.konv.airline.domain.service.AircraftSortingService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;
import java.util.Set;

@RestController
public class AircraftInfoRestController {

    private AircraftInfoService aircraftInfoService;
    private AircraftSortingService aircraftSortingService;

    @Inject
    public AircraftInfoRestController(AircraftInfoService aircraftInfoService, AircraftSortingService aircraftSortingService) {
        this.aircraftInfoService = aircraftInfoService;
        this.aircraftSortingService = aircraftSortingService;
    }

    @GetMapping(value = "/aircraft", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<? extends Aircraft> getAllAircraft(@RequestParam(value = "sort", defaultValue = "default") String sortingStrategy) {
        Set<? extends Aircraft> allAircraft = aircraftInfoService.getAllAircraft();
        return aircraftSortingService.sort(allAircraft, AircraftSortingService.SortingStrategy.forNameOrDefault(sortingStrategy));
    }

    @GetMapping(value = "/aircraft/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Aircraft getAircraftById(@PathVariable("id") long aircraftId) {
        return aircraftInfoService.getAircraftById(aircraftId).orElse(null);
    }

    @GetMapping(value = "/aircraft/search", produces = MediaType.APPLICATION_JSON_VALUE)
    public Set<? extends Aircraft> searchAircraft(
            @RequestParam(value = "min_passengers", defaultValue = "0") int minPassengerCapacity,
            @RequestParam(value = "max_passengers", defaultValue = Integer.MAX_VALUE + "") int maxPassengerCapacity,
            @RequestParam(value = "from_flight_range", defaultValue = "0") int minFlightRange,
            @RequestParam(value = "to_flight_range", defaultValue = Integer.MAX_VALUE + "") int maxFlightRange,
            @RequestParam(value = "min_fuel_consumption", defaultValue = "0") int minFuelConsumption,
            @RequestParam(value = "max_fuel_consumption", defaultValue = Integer.MAX_VALUE + "") int maxFuelConsumption
    ) {
        return aircraftInfoService.searchAircraft(aircraft ->
                (aircraft.getPassengerCapacity() >= minPassengerCapacity) &&
                        (aircraft.getPassengerCapacity() <= maxPassengerCapacity) &&
                        (aircraft.getMaxFlightRangeKm() >= minFlightRange) &&
                        (aircraft.getMaxFlightRangeKm() <= maxFlightRange) &&
                        (aircraft.getFuelConsumptionPerKm() >= minFuelConsumption) &&
                        (aircraft.getFuelConsumptionPerKm() <= maxFuelConsumption)
        );
    }
}
