package io.konv.airline.data.hsqldb;

import io.konv.airline.data.dao.AircraftDao;
import io.konv.airline.data.dto.AircraftTableRow;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public class HsqldbAircraftDao implements AircraftDao {

    private NamedParameterJdbcTemplate jdbcTemplate;
    private RowMapper<AircraftTableRow> aircraftTableRowMapper;

    @Inject
    public HsqldbAircraftDao(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        aircraftTableRowMapper = new BeanPropertyRowMapper<>(AircraftTableRow.class);
    }

    @Override
    public Set<AircraftTableRow> getAllAircraft() {
        String sql = "SELECT * FROM aircraft";
        List<AircraftTableRow> queryResult = jdbcTemplate.query(sql, aircraftTableRowMapper);
        return new HashSet<>(queryResult);
    }

    @Override
    public Optional<AircraftTableRow> getAircraftById(long aircraftId) {
        String sql = "SELECT * FROM aircraft WHERE id = :aircraftId";
        SqlParameterSource namedParameters = new MapSqlParameterSource("aircraftId", aircraftId);
        try {
            AircraftTableRow queryResult = jdbcTemplate.queryForObject(sql, namedParameters, aircraftTableRowMapper);
            return Optional.of(queryResult);
        } catch (IncorrectResultSizeDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Set<AircraftTableRow> getAircraftByAirlineId(long airlineId) {
        String sql = "SELECT * FROM aircraft WHERE airline_id = :airlineId";
        SqlParameterSource namedParameters = new MapSqlParameterSource("airlineId", airlineId);
        List<AircraftTableRow> queryResult = jdbcTemplate.query(sql, namedParameters, aircraftTableRowMapper);
        return new HashSet<>(queryResult);
    }

    @Override
    public boolean deleteAircraftById(long aircraftId) {
        String sql = "DELETE FROM aircraft WHERE id = :aircraftId";
        SqlParameterSource namedParameters = new MapSqlParameterSource("aircraftId", aircraftId);
        return jdbcTemplate.update(sql, namedParameters) > 0;
    }

    @Override
    public boolean insertAircraftOrUpdate(AircraftTableRow tableRow) {
        String sql = "INSERT INTO aircraft VALUES(" +
                ":id, :airlineId, :type, :name, :passengerCapacity, :cargoCapacity, :maxFlightRange, " +
                ":remainingFuel, :fuelConsumption, :wingspan, :maxRevolutions, :gasVolume)";

        MapSqlParameterSource namedParameters = new MapSqlParameterSource();

        namedParameters.addValue("id", tableRow.getId() >= 0 ? tableRow.getId() : "DEFAULT");
        namedParameters.addValue("airlineId", tableRow.getAirlineId());
        namedParameters.addValue("type", tableRow.getType());
        namedParameters.addValue("name", tableRow.getName());
        namedParameters.addValue("passengerCapacity", tableRow.getPassengerCapacity());
        namedParameters.addValue("cargoCapacity", tableRow.getCargoCapacity());
        namedParameters.addValue("maxFlightRange", tableRow.getMaxFlightRange());
        namedParameters.addValue("remainingFuel", tableRow.getRemainingFuel());
        namedParameters.addValue("fuelConsumption", tableRow.getFuelConsumption());
        namedParameters.addValue("wingspan", tableRow.getWingspan());
        namedParameters.addValue("maxRevolutions", tableRow.getMaxRevolutions());
        namedParameters.addValue("gasVolume", tableRow.getGasVolume());

        return jdbcTemplate.update(sql, namedParameters) > 0;
    }
}
