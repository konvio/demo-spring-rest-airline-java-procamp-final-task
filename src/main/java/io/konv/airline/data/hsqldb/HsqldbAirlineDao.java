package io.konv.airline.data.hsqldb;

import io.konv.airline.data.dao.AirlineDao;
import io.konv.airline.data.dto.AirlineTableRow;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public class HsqldbAirlineDao implements AirlineDao {

    private NamedParameterJdbcTemplate jdbcTemplate;
    private RowMapper<AirlineTableRow> airlineTableRowRowMapper;

    @Inject
    public HsqldbAirlineDao(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        airlineTableRowRowMapper = new BeanPropertyRowMapper<>(AirlineTableRow.class);
    }

    @Override
    public Set<AirlineTableRow> getAllAirlines() {
        String sql = "SELECT id, name FROM airline";
        List<AirlineTableRow> queryResult = jdbcTemplate.query(sql, airlineTableRowRowMapper);
        return new HashSet<>(queryResult);
    }

    @Override
    public Optional<AirlineTableRow> getAirlineById(long airlineId) {
        String sql = "SELECT id, name FROM airline WHERE id = :airlineId";
        SqlParameterSource namedParameters = new MapSqlParameterSource("airlineId", airlineId);
        try {
            AirlineTableRow queryResult = jdbcTemplate.queryForObject(sql, namedParameters, airlineTableRowRowMapper);
            return Optional.of(queryResult);
        } catch (IncorrectResultSizeDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public boolean insertAirlineOrUpdate(AirlineTableRow tableRow) {
        String sql = "INSERT INTO airline(id, name) VALUES (:id, :name)";

        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("id", tableRow.getId() >= 0 ? tableRow.getId() : "DEFAULT");
        namedParameters.addValue("name", tableRow.getName());

        return jdbcTemplate.update(sql, namedParameters) > 0;
    }

    @Override
    public boolean deleteAirlineById(long airlineId) {
        String sql = "DELETE FROM airline WHERE id = :airlineId";
        SqlParameterSource namedParameters = new MapSqlParameterSource("airlineId", airlineId);
        return jdbcTemplate.update(sql, namedParameters) > 0;
    }
}
