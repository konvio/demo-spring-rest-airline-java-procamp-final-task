package io.konv.airline.data.dto;

public class AircraftTableRow {

    private long id;
    private long airlineId;
    private String type;
    private String name;
    private int passengerCapacity;
    private int cargoCapacity;
    private int maxFlightRange;
    private int remainingFuel;
    private int fuelConsumption;
    private int wingspan;
    private int maxRevolutions;
    private int gasVolume;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getAirlineId() {
        return airlineId;
    }

    public void setAirlineId(long airlineId) {
        this.airlineId = airlineId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    public void setPassengerCapacity(int passengerCapacity) {
        this.passengerCapacity = passengerCapacity;
    }

    public int getCargoCapacity() {
        return cargoCapacity;
    }

    public void setCargoCapacity(int cargoCapacity) {
        this.cargoCapacity = cargoCapacity;
    }

    public int getMaxFlightRange() {
        return maxFlightRange;
    }

    public void setMaxFlightRange(int maxFlightRange) {
        this.maxFlightRange = maxFlightRange;
    }

    public int getRemainingFuel() {
        return remainingFuel;
    }

    public void setRemainingFuel(int remainingFuel) {
        this.remainingFuel = remainingFuel;
    }

    public int getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(int fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public int getWingspan() {
        return wingspan;
    }

    public void setWingspan(int wingspan) {
        this.wingspan = wingspan;
    }

    public int getMaxRevolutions() {
        return maxRevolutions;
    }

    public void setMaxRevolutions(int maxRevolutions) {
        this.maxRevolutions = maxRevolutions;
    }

    public int getGasVolume() {
        return gasVolume;
    }

    public void setGasVolume(int gasVolume) {
        this.gasVolume = gasVolume;
    }
}
