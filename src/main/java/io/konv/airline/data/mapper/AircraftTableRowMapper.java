package io.konv.airline.data.mapper;

import io.konv.airline.data.dto.AircraftTableRow;
import io.konv.airline.domain.entities.aircraft.*;

public final class AircraftTableRowMapper {

    private AircraftTableRowMapper() {
        throw new AssertionError("Class is not instantiable");
    }

    public static Aircraft convertTableRowToAircraft(AircraftTableRow tableRow) {

        Aircraft aircraft;

        switch (getAircraftTypeByName(tableRow.getType())) {
            case AIRPLANE:
                Airplane airplane = new Airplane();
                airplane.setWingspanSm(tableRow.getWingspan());
                aircraft = airplane;
                break;
            case BALLOON:
                Balloon balloon = new Balloon();
                balloon.setGasVolume(tableRow.getGasVolume());
                aircraft = balloon;
                break;
            case HELICOPTER:
                Helicopter helicopter = new Helicopter();
                helicopter.setMaxRevolutionsPerMinute(tableRow.getMaxRevolutions());
                aircraft = helicopter;
                break;
            default:
                aircraft = new GeneralAircraft();
        }

        aircraft.setId(tableRow.getId());
        aircraft.setName(tableRow.getName());
        aircraft.setPassengerCapacity(tableRow.getPassengerCapacity());
        aircraft.setCarryingCapacityKg(tableRow.getCargoCapacity());
        aircraft.setMaxFlightRangeKm(tableRow.getMaxFlightRange());
        aircraft.setRemainingFuel(tableRow.getRemainingFuel());
        aircraft.setFuelConsumptionPerKm(tableRow.getFuelConsumption());

        return aircraft;
    }

    public static AircraftTableRow convertAircraftToTableRow(Aircraft aircraft, long airlineId) {
        AircraftTableRow tableRow = new AircraftTableRow();

        tableRow.setId(aircraft.getId());
        tableRow.setAirlineId(airlineId);
        tableRow.setName(aircraft.getName());
        tableRow.setPassengerCapacity(aircraft.getPassengerCapacity());
        tableRow.setCargoCapacity(aircraft.getCarryingCapacityKg());
        tableRow.setMaxFlightRange(aircraft.getMaxFlightRangeKm());
        tableRow.setRemainingFuel(aircraft.getRemainingFuel());
        tableRow.setFuelConsumption(aircraft.getFuelConsumptionPerKm());

        if (aircraft instanceof Airplane) {
            Airplane airplane = (Airplane) aircraft;
            tableRow.setType(AircraftType.AIRPLANE.getTypeName());
            tableRow.setWingspan(airplane.getWingspanSm());
        } else if (aircraft instanceof Balloon) {
            Balloon balloon = (Balloon) aircraft;
            tableRow.setType(AircraftType.BALLOON.getTypeName());
            tableRow.setGasVolume(balloon.getGasVolume());
        } else if (aircraft instanceof Helicopter) {
            Helicopter helicopter = (Helicopter) aircraft;
            tableRow.setType(AircraftType.HELICOPTER.getTypeName());
            tableRow.setMaxRevolutions(helicopter.getMaxRevolutionsPerMinute());
        } else {
            tableRow.setType(AircraftType.DEFAULT.getTypeName());
        }

        return tableRow;
    }

    private static AircraftType getAircraftTypeByName(String typeName) {
        for (AircraftType aircraftType : AircraftType.values()) {
            if (aircraftType.getTypeName().equals(typeName)) return aircraftType;
        }
        return AircraftType.DEFAULT;
    }

    private enum AircraftType {
        AIRPLANE("airplane"), BALLOON("balloon"), HELICOPTER("helicopter"), DEFAULT("");

        private String typeName;

        AircraftType(String typeName) {
            this.typeName = typeName;
        }

        public String getTypeName() {
            return typeName;
        }
    }
}
