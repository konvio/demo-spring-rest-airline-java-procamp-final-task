package io.konv.airline.data.mapper;

import io.konv.airline.data.dto.AirlineTableRow;
import io.konv.airline.domain.entities.airline.Airline;
import io.konv.airline.domain.entities.airline.SimpleAirline;

public final class AirlineTableRowMapper {

    private AirlineTableRowMapper() {
        throw new AssertionError("Class in not instantiable");
    }

    public static Airline convertTableRowToAirline(AirlineTableRow tableRow) {
        Airline airline = new SimpleAirline();
        airline.setId(tableRow.getId());
        airline.setName(tableRow.getName());
        return airline;
    }

    public static AirlineTableRow convertAirlineToTableRow(Airline airline) {
        AirlineTableRow tableRow = new AirlineTableRow();
        tableRow.setId(airline.getId());
        tableRow.setName(airline.getName());
        return tableRow;
    }
}
