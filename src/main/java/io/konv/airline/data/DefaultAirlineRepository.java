package io.konv.airline.data;

import io.konv.airline.data.dao.AircraftDao;
import io.konv.airline.data.dao.AirlineDao;
import io.konv.airline.data.dto.AircraftTableRow;
import io.konv.airline.data.mapper.AircraftTableRowMapper;
import io.konv.airline.data.mapper.AirlineTableRowMapper;
import io.konv.airline.domain.AirlineRepository;
import io.konv.airline.domain.entities.aircraft.Aircraft;
import io.konv.airline.domain.entities.airline.Airline;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
public class DefaultAirlineRepository implements AirlineRepository {

    private AirlineDao airlineDao;
    private AircraftDao aircraftDao;

    @Inject
    public DefaultAirlineRepository(AirlineDao airlineDao, AircraftDao aircraftDao) {
        this.airlineDao = airlineDao;
        this.aircraftDao = aircraftDao;
    }

    @Override
    public Set<? extends Airline> getAllAirlines() {
        return airlineDao.getAllAirlines().stream()
                .map(AirlineTableRowMapper::convertTableRowToAirline)
                .map(this::populateAirlineWithAircraft)
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @Override
    public Set<? extends Aircraft> getAllAircraft() {
        return aircraftDao.getAllAircraft().stream()
                .map(AircraftTableRowMapper::convertTableRowToAircraft)
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @Override
    public Optional<? extends Aircraft> getAircraftById(long aircraftId) {
        return aircraftDao.getAircraftById(aircraftId)
                .map(AircraftTableRowMapper::convertTableRowToAircraft);
    }

    @Override
    public boolean addAirlineOrUpdate(Airline airline) {
        boolean airlineInserted = airlineDao.insertAirlineOrUpdate(
                AirlineTableRowMapper.convertAirlineToTableRow(airline)
        );
        airline.getOwnedAircraft().stream()
                .map(aircraft -> AircraftTableRowMapper.convertAircraftToTableRow(aircraft, airline.getId()))
                .forEach(aircraftDao::insertAircraftOrUpdate);
        return airlineInserted;
    }

    @Override
    public boolean addAircraftToAirlineOrUpdate(Aircraft aircraft, long airlineId) {
        AircraftTableRow aircraftTableRow = AircraftTableRowMapper.convertAircraftToTableRow(aircraft, airlineId);
        return aircraftDao.insertAircraftOrUpdate(aircraftTableRow);
    }

    @Override
    public boolean deleteAirlineIfPresent(long airlineId) {
        return airlineDao.deleteAirlineById(airlineId);
    }

    @Override
    public boolean deleteAircraftIfPresent(long aircraftId) {
        return aircraftDao.deleteAircraftById(aircraftId);
    }

    @Override
    public Optional<Set<? extends Aircraft>> getAircraftByAirlineId(long airlineId) {
        Set<Aircraft> aircraftSet = aircraftDao.getAircraftByAirlineId(airlineId).stream()
                .map(AircraftTableRowMapper::convertTableRowToAircraft)
                .collect(Collectors.toSet());
        return aircraftSet.isEmpty() ? Optional.empty() : Optional.of(aircraftSet);
    }

    private Airline populateAirlineWithAircraft(Airline airline) {
        aircraftDao.getAircraftByAirlineId(airline.getId()).stream()
                .map(AircraftTableRowMapper::convertTableRowToAircraft)
                .forEach(airline::addAircraft);
        return airline;
    }
}
