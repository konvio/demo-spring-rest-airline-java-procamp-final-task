package io.konv.airline.data.dao;

import io.konv.airline.data.dto.AircraftTableRow;

import java.util.Optional;
import java.util.Set;

public interface AircraftDao {

    Set<AircraftTableRow> getAllAircraft();

    Optional<AircraftTableRow> getAircraftById(long aircraftId);

    Set<AircraftTableRow> getAircraftByAirlineId(long airlineId);

    boolean deleteAircraftById(long aircraftId);

    boolean insertAircraftOrUpdate(AircraftTableRow tableRow);
}