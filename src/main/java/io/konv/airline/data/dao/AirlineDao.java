package io.konv.airline.data.dao;

import io.konv.airline.data.dto.AirlineTableRow;

import java.util.Optional;
import java.util.Set;

public interface AirlineDao {

    Set<AirlineTableRow> getAllAirlines();

    Optional<AirlineTableRow> getAirlineById(long airlineId);

    boolean insertAirlineOrUpdate(AirlineTableRow tableRow);

    boolean deleteAirlineById(long airlineId);
}
