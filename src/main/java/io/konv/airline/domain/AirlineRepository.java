package io.konv.airline.domain;

import io.konv.airline.domain.entities.aircraft.Aircraft;
import io.konv.airline.domain.entities.airline.Airline;

import java.util.Optional;
import java.util.Set;

public interface AirlineRepository {

    Set<? extends Airline> getAllAirlines();

    Set<? extends Aircraft> getAllAircraft();

    Optional<? extends Aircraft> getAircraftById(long aircraftId);

    boolean addAirlineOrUpdate(Airline airline);

    boolean addAircraftToAirlineOrUpdate(Aircraft aircraft, long airlineId);

    boolean deleteAirlineIfPresent(long airlineId);

    boolean deleteAircraftIfPresent(long aircraftId);

    Optional<Set<? extends Aircraft>> getAircraftByAirlineId(long airlineId);
}
