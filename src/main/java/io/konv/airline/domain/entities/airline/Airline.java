package io.konv.airline.domain.entities.airline;

import io.konv.airline.domain.entities.aircraft.Aircraft;

import java.util.Set;

public interface Airline {

    long getId();

    void setId(long id);

    String getName();

    void setName(String name);

    Set<? extends Aircraft> getOwnedAircraft();

    boolean removeAircraft(Aircraft aircraft);

    boolean addAircraft(Aircraft aircraft);
}
