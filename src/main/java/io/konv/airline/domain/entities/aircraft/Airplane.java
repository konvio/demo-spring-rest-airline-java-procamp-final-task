package io.konv.airline.domain.entities.aircraft;

import javax.validation.constraints.PositiveOrZero;

public class Airplane extends GeneralAircraft {

    @PositiveOrZero
    private int wingspanSm;

    public int getWingspanSm() {
        return wingspanSm;
    }

    public void setWingspanSm(int wingspanSm) {
        this.wingspanSm = wingspanSm;
    }
}
