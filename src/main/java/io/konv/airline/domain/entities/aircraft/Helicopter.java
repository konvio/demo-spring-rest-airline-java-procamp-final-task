package io.konv.airline.domain.entities.aircraft;

import javax.validation.constraints.PositiveOrZero;

public class Helicopter extends GeneralAircraft {

    @PositiveOrZero
    private int maxRevolutionsPerMinute;

    public int getMaxRevolutionsPerMinute() {
        return maxRevolutionsPerMinute;
    }

    public void setMaxRevolutionsPerMinute(int maxRevolutionsPerMinute) {
        this.maxRevolutionsPerMinute = maxRevolutionsPerMinute;
    }
}
