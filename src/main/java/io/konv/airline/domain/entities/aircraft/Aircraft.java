package io.konv.airline.domain.entities.aircraft;

public interface Aircraft {

    long getId();

    void setId(long id);

    String getName();

    void setName(String name);

    int getPassengerCapacity();

    void setPassengerCapacity(int passengerCapacity);

    int getCarryingCapacityKg();

    void setCarryingCapacityKg(int carryingCapacityKg);

    int getMaxFlightRangeKm();

    void setMaxFlightRangeKm(int maxFlightRangeKm);

    int getRemainingFuel();

    void setRemainingFuel(int remainingFuel);

    int getFuelConsumptionPerKm();

    void setFuelConsumptionPerKm(int fuelConsumptionPerKm);
}
