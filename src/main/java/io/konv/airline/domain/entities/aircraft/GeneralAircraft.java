package io.konv.airline.domain.entities.aircraft;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class GeneralAircraft implements Aircraft {

    private long id;

    @NotBlank
    @Size(max = 32)
    private String name = "Unnamed Aircraft";

    @PositiveOrZero
    private int passengerCapacity;

    @PositiveOrZero
    private int carryingCapacityKg;

    @PositiveOrZero
    private int maxFlightRangeKm;

    @PositiveOrZero
    private int remainingFuel;

    @PositiveOrZero
    private int fuelConsumptionPerKm = 0;

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    @Override
    public void setPassengerCapacity(int passengerCapacity) {
        this.passengerCapacity = passengerCapacity;
    }

    @Override
    public int getCarryingCapacityKg() {
        return carryingCapacityKg;
    }

    @Override
    public void setCarryingCapacityKg(int carryingCapacityKg) {
        this.carryingCapacityKg = carryingCapacityKg;
    }

    @Override
    public int getMaxFlightRangeKm() {
        return maxFlightRangeKm;
    }

    @Override
    public void setMaxFlightRangeKm(int maxFlightRangeKm) {
        this.maxFlightRangeKm = maxFlightRangeKm;
    }

    @Override
    public int getRemainingFuel() {
        return remainingFuel;
    }

    @Override
    public void setRemainingFuel(int remainingFuel) {
        this.remainingFuel = remainingFuel;
    }

    @Override
    public int getFuelConsumptionPerKm() {
        return fuelConsumptionPerKm;
    }

    @Override
    public void setFuelConsumptionPerKm(int fuelConsumptionPerKm) {
        this.fuelConsumptionPerKm = fuelConsumptionPerKm;
    }
}
