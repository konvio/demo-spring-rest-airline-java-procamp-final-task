package io.konv.airline.domain.entities.aircraft;

import javax.validation.constraints.PositiveOrZero;

public class Balloon extends GeneralAircraft {

    @PositiveOrZero
    private int gasVolume;

    public int getGasVolume() {
        return gasVolume;
    }

    public void setGasVolume(int gasVolume) {
        this.gasVolume = gasVolume;
    }
}
