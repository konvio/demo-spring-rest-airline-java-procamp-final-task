package io.konv.airline.domain.entities.airline;

import io.konv.airline.domain.entities.aircraft.Aircraft;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

public class SimpleAirline implements Airline {

    private long id;

    @NotBlank
    @Size(max = 32)
    private String name;

    private Set<Aircraft> ownedAircraft = new LinkedHashSet<>();

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Set<? extends Aircraft> getOwnedAircraft() {
        return Collections.unmodifiableSet(ownedAircraft);
    }

    @Override
    public boolean removeAircraft(Aircraft aircraft) {
        return ownedAircraft.remove(aircraft);
    }

    @Override
    public boolean addAircraft(Aircraft aircraft) {
        return ownedAircraft.add(aircraft);
    }
}
