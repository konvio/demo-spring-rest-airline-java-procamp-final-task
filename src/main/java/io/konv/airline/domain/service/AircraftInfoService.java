package io.konv.airline.domain.service;

import io.konv.airline.domain.entities.aircraft.Aircraft;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public interface AircraftInfoService {

    Set<? extends Aircraft> getAllAircraft();

    Optional<? extends Aircraft> getAircraftById(long aircraftId);

    default Set<? extends Aircraft> searchAircraft(Predicate<Aircraft> predicate) {
        return getAllAircraft().stream()
                .filter(predicate)
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }
}
