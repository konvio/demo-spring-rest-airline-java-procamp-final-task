package io.konv.airline.domain.service.impl;

import io.konv.airline.domain.AirlineRepository;
import io.konv.airline.domain.entities.aircraft.Aircraft;
import io.konv.airline.domain.entities.airline.Airline;
import io.konv.airline.domain.service.AirlineInfoService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class DefaultAirlineInfoService implements AirlineInfoService {

    private AirlineRepository repository;

    @Inject
    public DefaultAirlineInfoService(AirlineRepository repository) {
        this.repository = repository;
    }

    @Override
    public Set<? extends Airline> getAllAirlines() {
        return repository.getAllAirlines();
    }

    @Override
    public Optional<? extends Airline> getAirlineById(long airlineId) {
        return repository.getAllAirlines().stream()
                .filter(airline -> airline.getId() == airlineId)
                .findFirst();
    }

    @Override
    public Optional<Integer> getTotalPassengerCapacity(long airlineId) {
        return repository.getAircraftByAirlineId(airlineId).map(ownedAircraft ->
                ownedAircraft.stream()
                        .mapToInt(Aircraft::getPassengerCapacity)
                        .sum()
        );
    }

    @Override
    public Optional<Integer> getTotalCarryingCapacityKg(long airlineId) {
        return repository.getAircraftByAirlineId(airlineId).map(ownedAircraft ->
                ownedAircraft.stream()
                        .mapToInt(Aircraft::getCarryingCapacityKg)
                        .sum()
        );
    }

    @Override
    public Optional<Set<? extends Aircraft>> getAircraftByAirlineId(long airlineId) {
        return repository.getAircraftByAirlineId(airlineId);
    }
}
