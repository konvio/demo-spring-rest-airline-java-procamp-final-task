package io.konv.airline.domain.service;

import io.konv.airline.domain.entities.aircraft.Aircraft;

import java.util.Collection;
import java.util.List;

public interface AircraftSortingService {

    List<? extends Aircraft> sort(Collection<? extends Aircraft> aircraft, SortingStrategy strategy);

    enum SortingStrategy {
        DEFAULT("default"), FLIGHT_RANGE_ASC("flight_range");

        private String strategyName;

        SortingStrategy(String strategyName) {
            this.strategyName = strategyName;
        }

        public String getStrategyName() {
            return strategyName;
        }

        public static SortingStrategy forNameOrDefault(String strategyName) {
            for (SortingStrategy strategy : SortingStrategy.values()) {
                if (strategy.getStrategyName().equals(strategyName)) return strategy;
            }
            return SortingStrategy.DEFAULT;
        }
    }
}
