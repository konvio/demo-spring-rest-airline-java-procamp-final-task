package io.konv.airline.domain.service.impl;

import io.konv.airline.data.dto.AircraftTableRow;
import io.konv.airline.data.mapper.AircraftTableRowMapper;
import io.konv.airline.domain.AirlineRepository;
import io.konv.airline.domain.entities.aircraft.Aircraft;
import io.konv.airline.domain.service.AircraftModificationService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class DefaultAircraftModificationService implements AircraftModificationService {

    private AirlineRepository airlineRepository;

    @Inject
    public DefaultAircraftModificationService(AirlineRepository airlineRepository) {
        this.airlineRepository = airlineRepository;
    }

    @Override
    public boolean deleteAircraftIfPresent(long aircraftId) {
        return airlineRepository.deleteAircraftIfPresent(aircraftId);
    }

    @Override
    public boolean addAircraft(AircraftTableRow aircraftTableRow) {
        Aircraft aircraft = AircraftTableRowMapper.convertTableRowToAircraft(aircraftTableRow);
        return airlineRepository.addAircraftToAirlineOrUpdate(aircraft, aircraftTableRow.getAirlineId());
    }
}
