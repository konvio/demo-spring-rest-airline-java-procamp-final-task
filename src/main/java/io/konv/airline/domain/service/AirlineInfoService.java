package io.konv.airline.domain.service;

import io.konv.airline.domain.entities.aircraft.Aircraft;
import io.konv.airline.domain.entities.airline.Airline;

import java.util.Optional;
import java.util.Set;

public interface AirlineInfoService {

    Set<? extends Airline> getAllAirlines();

    Optional<? extends Airline> getAirlineById(long airlineId);

    Optional<Integer> getTotalPassengerCapacity(long airlineId);

    Optional<Integer> getTotalCarryingCapacityKg(long airlineId);

    Optional<Set<? extends Aircraft>> getAircraftByAirlineId(long airlineId);
}
