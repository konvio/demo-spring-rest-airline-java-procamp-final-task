package io.konv.airline.domain.service;

import io.konv.airline.data.dto.AircraftTableRow;

public interface AircraftModificationService {

    boolean deleteAircraftIfPresent(long aircraftId);

    //TODO: Remove coupling with data layer
    boolean addAircraft(AircraftTableRow aircraft);
}
