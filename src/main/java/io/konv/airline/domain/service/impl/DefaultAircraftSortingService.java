package io.konv.airline.domain.service.impl;

import io.konv.airline.domain.entities.aircraft.Aircraft;
import io.konv.airline.domain.service.AircraftSortingService;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DefaultAircraftSortingService implements AircraftSortingService {

    @Override
    public List<? extends Aircraft> sort(Collection<? extends Aircraft> aircraft, SortingStrategy strategy) {
        Comparator<Aircraft> comparator;
        switch (strategy) {
            case FLIGHT_RANGE_ASC:
                comparator = Comparator.comparingInt(Aircraft::getMaxFlightRangeKm);
                break;
            default:
            case DEFAULT:
                comparator = Comparator.comparingLong(Aircraft::getId);
                break;
        }

        return aircraft.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }
}
