package io.konv.airline.domain.service.impl;

import io.konv.airline.domain.AirlineRepository;
import io.konv.airline.domain.entities.aircraft.Aircraft;
import io.konv.airline.domain.service.AircraftInfoService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Optional;
import java.util.Set;

@Service
public class DefaultAircraftInfoService implements AircraftInfoService {

    private AirlineRepository repository;

    @Inject
    public DefaultAircraftInfoService(AirlineRepository repository) {
        this.repository = repository;
    }

    @Override
    public Set<? extends Aircraft> getAllAircraft() {
        return repository.getAllAircraft();
    }

    @Override
    public Optional<? extends Aircraft> getAircraftById(long aircraftId) {
        return repository.getAircraftById(aircraftId);
    }
}
