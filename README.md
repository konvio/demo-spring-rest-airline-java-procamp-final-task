# Airline REST Service

## Installation

1. Make sure port 8080 on localhost is free
2. Run the following command

```bash
git clone https://konvio@bitbucket.org/konvio/demo-spring-rest-airline-java-procamp-final-task.git && cd demo-spring-rest-airline-java-procamp-final-task && gradle bootJar && java -jar build/libs/airline-0.0.1-SNAPSHOT.jar
```

## Test Report

1. Navigate to the source directory
2. Run the following command (Linux)

```bash
gradle test && gradle jacocoTestReport && sensible-browser build/reports/jacoco/test/html/index.html
```

## Example URLs
1. [All aircraft](http://localhost:8080/aircraft)
2. [All aircraft sorted by flight range](http://localhost:8080/aircraft?sort=flight_range)
3. [Aircraft with id=7](http://localhost:8080/aircraft/1)
4. [All airlines](http://localhost:8080/airline)
5. [All aircrafts in the airline with id=1 sorted by flight range](http://localhost:8080/airline/1/aircraft?sort=flight_range)
6. [All aircraft with minimum passenger capacity 100 and fuel consumption from 20 to 50](http://localhost:8080/aircraft/search?min_passengers=100&min_fuel_consumption=20&max_fuel_consumption=50)
7. Other are available from rest controllers request mapping annotations